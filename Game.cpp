#include "Game.h"
#include <vector>
#include <cmath>
#include <typeinfo>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;
RenderWindow window(VideoMode(800, 600), "Arkanoid");

Game::Game(void)
{
	phase = END;

	if (!font.loadFromFile("Arial.ttf"))
	{
		//MessageBox(NULL, "B??d czcionki!", "ERROR", NULL);
		return;
	}

	phase = MENU;
}
void Game::about() {
	while (true)
	{
		if (Keyboard::isKeyPressed(Keyboard::Escape)) {
			phase = MENU;
			startGame();
		}
		window.clear();

		Text title("Game Developers", font, 50);
		title.setStyle(Text::Bold);
		title.setColor(Color(10, 10, 255));
		title.setPosition(800 / 2 - title.getGlobalBounds().width / 2, 50);

		const int rozmiar = 3;

		Text tablica[rozmiar];

		string option2[] = { "Oksana Stechkevych", "Leon Jaworski", "Wojtek Pacek" };

		for (int i = 0; i < rozmiar; i++)
		{
			tablica[i].setFont(font);
			tablica[i].setCharacterSize(30);
			tablica[i].setColor(Color(50, 50, 50));
			tablica[i].setString(option2[i]);
			tablica[i].setPosition(800 / 2 - title.getGlobalBounds().width / 2, 150 + i * 100);
		}

		window.clear(Color(245, 245, 255));
		window.draw(title);
		for (int i = 0; i < rozmiar; i++)
		{
			window.draw(tablica[i]);
		}
		window.display();

	}
}

//int maxScore;

void Game::graj() {
	Ball ball(MaxWidth / 2, MaxHeight / 2);
	ifstream scorefile2;
	scorefile2.open("highscore.txt", ios::in);
	ball.textura.loadFromFile("ball.png");
	ball.textura.setSmooth(true);
	ball.shape.setTexture(&ball.textura);
	Paddle paddle(350, 550);
	Brick brick(0, 0);
	int increaseScore = 0;
	int maxScore = 0;
	scorefile2 >> maxScore;
	scorefile2.close();
	vector<Brick> bricks;																	//dodaje klocki
	for (int i = 0; i < brickX; i++)
		for (int j = 0; j < brickY; j++)
			bricks.emplace_back((i + 2)*(blockWidth + 3),
				(j + 3)*(blockHeight + 3));
	window.setFramerateLimit(60);

	while (!ball.lost)
	{
		window.clear(Color(245, 245, 255));
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			phase = MENU;
			break;
		}
		ball.update();
		ball.lifes.update();
		paddle.update();

		testCollide(paddle, ball);

		window.draw(ball.shape);
		window.draw(paddle.shape);

		for (Brick& brick : bricks) 														//wyswietla klocki
			window.draw(brick.shape);

		for (int i = 0; i < LIFES - ball.lifes.lostlifes; i++) {
			window.draw(ball.lifes.life[i]);
			//window.draw(your_score);
		}

		for (Brick& brick : bricks) {
			testCollide(brick, ball);
			if (brick.striked == true)
				increaseScore += 50;
			Text your_score("Score   " + to_string(increaseScore), font, 20);
			your_score.setPosition(180, 570);
			your_score.setColor(Color(185, 0, 135));
			window.draw(your_score);
		}

		bricks.erase(remove_if(begin(bricks), end(bricks), [](const Brick& bricks) { return bricks.striked; }), end(bricks));

		if (bricks.empty()) {
			phase = MENU;
			ball.speed = { 0,0 };

			Text textwon("You won!", font, 60);
			textwon.setStyle(Text::Bold);
			textwon.setColor(Color(17, 73, 171));
			textwon.setPosition(290, 250);
			if (increaseScore > maxScore) {
				ofstream scorefile4("highscore.txt", ios::trunc);
				scorefile4 << increaseScore;
				scorefile4.close();
			}

			Text High_score("Your score is:", font, 40);
			High_score.setColor(Color::Blue);
			High_score.setPosition(290, 330);
			Text High_score1(to_string(increaseScore), font, 40);
			High_score1.setColor(Color::Red);
			High_score1.setPosition(375, 400);

			window.draw(textwon);
			window.draw(High_score);
			window.draw(High_score1);

			window.display();

			while (true) {
				if (Keyboard::isKeyPressed(Keyboard::Return))
					break;
			}

			startGame();
		}
		window.display();

	}

	if (ball.lost) {
		window.clear();

		Text title3("Game over!", font, 50);
		title3.setStyle(Text::Bold);
		title3.setColor(Color(255, 0, 0));
		title3.setPosition(260, 200);

		Text High_score("Your score is: " + to_string(increaseScore), font, 40);
		High_score.setColor(Color::White);
		High_score.setPosition(240, 250);

		if (increaseScore > maxScore) {
			ofstream scorefile5("highscore.txt", ios::trunc);
			scorefile5 << increaseScore;
			scorefile5.close();
		}

		Text napis("Press enter", font, 30);
		napis.setColor(Color(255, 0, 0));
		napis.setPosition(MaxWidth / 2, MaxHeight / 2);

		window.draw(High_score);
		window.draw(napis);
		window.draw(title3);

		window.display();

		while (!Keyboard::isKeyPressed(Keyboard::Key::Return)) {
			;
		}
		phase = MENU;
		startGame();

	}

}

int maxScore;

void Game::highscore() {
	while (true)
	{
		if (Keyboard::isKeyPressed(Keyboard::Escape)) {
			phase = MENU;
			startGame();
		}
		window.clear();

		Text title("High score", font, 40);
		title.setStyle(Text::Bold);
		title.setColor(Color(255, 140, 0));
		title.setPosition(MaxWidth / 2 - 80, 50);

		ifstream score("highscore.txt", ios::in);
		int maxScore=0;
		score >> maxScore;
		score.close();
		//maxScore zaimplementowana w void Game::graj() wiec nie moze czytac tej zmiennej tu

		Text HighScoreValue(to_string(maxScore), font, 60);
		HighScoreValue.setPosition(MaxWidth / 2.f, 200);
		HighScoreValue.setColor(Color(185, 60, 60));

		window.clear(Color(245, 245, 255));
		window.draw(title);
		window.draw(HighScoreValue);
		window.display();
	}
}

void Game::startGame()
{
	while (phase != END)
	{
		switch (phase)
		{
		case MENU:
			menu();
			break;
		case GAME:
			graj();
			break;
		case ABOUT:
			about();
			break;
		case HIGHSCORE:
			highscore();
			break;
		}
	}
	window.close();
	return;
}

void Game::menu()
{
	Text title("Arkanoid", font, 50);
	title.setStyle(Text::Bold);
	title.setColor(Color(180, 253, 11));
	title.setPosition(800 / 2 - title.getGlobalBounds().width / 2, 50);

	const int how = 4;

	Text table[how];

	string option[] = { "Play", "High score", "About", "Exit" };
	for (int i = 0; i<how; i++)
	{
		table[i].setFont(font);
		table[i].setCharacterSize(30);

		table[i].setString(option[i]);
		table[i].setPosition(800 / 2 - table[i].getGlobalBounds().width / 2, 150 + i * 100);
	}

	while (phase == MENU)
	{
		Vector2f mouse(Mouse::getPosition(window));
		Event event;

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				phase = END;
			else if (table[0].getGlobalBounds().contains(mouse) &&
				event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
			{
				phase = GAME;
				startGame();
			}
			else if (table[1].getGlobalBounds().contains(mouse) &&
				event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
			{
				phase = HIGHSCORE;
				startGame();
			}
			else if (table[2].getGlobalBounds().contains(mouse) &&
				event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
			{
				phase = ABOUT;
				startGame();
			}

			else if (table[3].getGlobalBounds().contains(mouse) &&
				event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
			{
				phase = END;
				startGame();
			}
		}
		for (int i = 0; i<how; i++)
			if (table[i].getGlobalBounds().contains(mouse))
				table[i].setColor(Color::Cyan);
			else table[i].setColor(Color::White);

			window.clear();

			window.draw(title);
			for (int i = 0; i<how; i++)
				window.draw(table[i]);

			window.display();
	}
}

Lifes::Lifes() {
	lostlifes = 0;
	lifes = new int[LIFES];
	Texture textura;
	textura.loadFromFile("ball.png");
	textura.setSmooth(true);
	for (int i = 0; i < LIFES; i++) {
		life[i].setRadius(ballRadius);
		life[i].setFillColor(Color(149, 149, 228));
		life[i].setTexture(&textura);
	}
}

void Lifes::update() {
	int starty = 580;
	int startx = 20;
	for (int i = 0; i < LIFES - lostlifes; i++)
	{
		life[i].setPosition(startx, starty);
		startx += 30;
	}
}

Ball::Ball(float x, float y) {
	shape.setPosition(x, y);
	shape.setRadius(ballRadius);
	shape.setFillColor(Color::White);
	shape.setOrigin(ballRadius, ballRadius);

}
void Ball::reset() {
	shape.setPosition(MaxWidth / 2, MaxHeight / 2);
	speed = { 0, 0 };
	while (true) {
		if (Keyboard::isKeyPressed(Keyboard::Left) || Keyboard::isKeyPressed(Keyboard::Right)) {
			speed.x = ballSpeed;
			speed.y = -ballSpeed;
			break;
		}
	}
}
void Ball::update() {
	if ((LIFES - lifes.lostlifes) == 0) {
		speed = { 0, 0 };
		lost = true;
		// TUTAJ KOD KONCZACY GRE
	}
	shape.move(speed);
	if (left() < 0) speed.x = ballSpeed;
	else if (right() > MaxWidth) speed.x = -ballSpeed;

	if (top() < 0) speed.y = ballSpeed;
	else if (bottom() > MaxHeight) {
		lifes.lostlifes++;
		reset();
	}
}

float Ball::x() { return shape.getPosition().x; };
float Ball::y() { return shape.getPosition().y; };
float Ball::left() { return x() - shape.getRadius(); }
float Ball::right() { return x() + shape.getRadius(); }
float Ball::top() { return y() - shape.getRadius(); }
float Ball::bottom() { return y() + shape.getRadius(); }


Paddle::Paddle(float x, float y)
{
	shape.setPosition(x, y);
	shape.setSize({ paddleWidth, paddleHigh });
	shape.setFillColor(Color::Green);
	shape.setOrigin(paddleWidth / 2.f, paddleHigh / 2.f);
}

void Paddle::update()
{
	shape.move(speed);
	if (Keyboard::isKeyPressed(Keyboard::Key::Left) && left()>0)
	{
		speed.x = -paddle_speed;
	}
	else if (Keyboard::isKeyPressed(Keyboard::Key::Right) && right()< MaxWidth)
	{
		speed.x = paddle_speed;
	}
	else speed.x = 0;
}

float Paddle::x() { return shape.getPosition().x; }
float Paddle::y() { return shape.getPosition().y; }
float Paddle::left() { return x() - shape.getSize().x / 2.f; }
float Paddle::right() { return x() + shape.getSize().x / 2.f; }
float Paddle::top() { return y() - shape.getSize().y / 2.f; }
float Paddle::bottom() { return y() + shape.getSize().y / 2.f; }

template<class T1, class T2> bool isCollide(T1& a, T2& b)
{
	return a.right() >= b.left() && a.left() <= b.right() && a.bottom() >= b.top() && a.top() <= b.bottom();
}

void testCollide(Paddle& paddle, Ball& ball)
{
	if (!isCollide(paddle, ball)) return;

	ball.speed.y = -ballSpeed;

	if (ball.x() < paddle.x()) ball.speed.x = -ballSpeed;
	else ball.speed.x = ballSpeed;

}
void testCollide(Brick& brick, Ball& ball) {											//colliding dla pilki i klockow
	if (!isCollide(brick, ball)) return;

	brick.striked = true;

	float overlapRight{ ball.left() - brick.right() };
	float overlapLeft{ brick.left() - ball.right() };
	float overlapTop{ ball.bottom() - brick.top() };
	float overlapBottom{ brick.bottom() - ball.top() };

	bool ballFromRight(fabs(overlapRight) < fabs(overlapLeft));
	bool ballFromTop(fabs(overlapTop) < fabs(overlapBottom));

	float minOverlapX(ballFromRight ? overlapRight : overlapLeft);
	float minOverlapY(ballFromTop ? overlapTop : overlapBottom);

	if (abs(minOverlapX) < abs(minOverlapY))
		ball.speed.x = ballFromRight ? -ballSpeed : ballSpeed;
	else
		ball.speed.y = ballFromTop ? -ballSpeed : ballSpeed;

}

Brick::Brick(float x, float y) {
	shape.setPosition(x, y);
	shape.setSize({ blockWidth, blockHeight });
	shape.setFillColor(Color::Cyan);
	shape.setOrigin(blockWidth / 2.f, blockHeight / 2.f);
}


float Brick::x() { return shape.getPosition().x; };
float Brick::y() { return shape.getPosition().y; };
float Brick::right() { return x() + shape.getSize().x / 2.f; };
float Brick::left() { return x() - shape.getSize().x / 2.f; };
float Brick::top() { return y() - shape.getSize().y / 2.f; };
float Brick::bottom() { return y() + shape.getSize().y / 2.f; };