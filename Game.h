#include <SFML\Graphics.hpp>

#define ballRadius 10.f
#define ballSpeed 4.f
#define paddle_speed 7.f
#define MaxWidth 800
#define MaxHeight 600
#define paddleWidth 50
#define paddleHigh 10
#define LIFES 3
#define brickX 10
#define brickY 5
#define blockWidth 60
#define blockHeight 20

using namespace sf;

class Game
{
public:
	Game(void);

	void startGame();

protected:
	enum GameState { MENU, GAME, HIGHSCORE, GAME_OVER, ABOUT, END };
	GameState phase;

private:
	Font font;

	void menu();
	void graj();
	void about();
	void highscore();
};

class Lifes {
public:
	Lifes(void);
	int* lifes;
	CircleShape life[LIFES];
	int lostlifes;
	void update();
};

class Ball {
public:
	Ball(float, float);
	bool lost = false;
	Lifes lifes;
	Texture textura;
	CircleShape shape;
	Vector2f speed{ ballSpeed, -ballSpeed };

	void reset();
	void update();

	float x();
	float y();
	float right();
	float left();
	float top();
	float bottom();

};

class Paddle {
public:
	Paddle(float, float);
	RectangleShape shape;
	Vector2f speed;

	void update();
	float x();
	float y();
	float right();
	float left();
	float top();
	float bottom();
};

class Brick {
public:
	Brick(float, float);
	RectangleShape shape;
	bool striked{ false };

	float x();
	float y();
	float right();
	float left();
	float top();
	float bottom();
};


template<class T1, class T2> bool isCollide(T1&, T2&);

void testCollide(Paddle&, Ball&);
void testCollide(Brick&, Ball&);